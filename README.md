# TheraVerse: AI-Driven Virtual Reality Therapy

## Description

TheraVerse is an AI-enhanced virtual reality (VR) platform designed for therapeutic applications, including exposure therapy for phobias, pain management, and rehabilitation. The platform leverages machine learning to personalize and adapt therapy sessions, ensuring a tailored experience for each user.

![Home Page](images/Home.png)

![Login Page](images/Login.png)

![Therapy Selection](images/Therapy_Selection.png)

## Components

1. [VR Development](#vr-development)
2. [AI/ML Model Development](#aiml-model-development)
3. [Backend Development](#backend-development)
4. [Frontend Development](#frontend-development)
5. [Database and Data Management](#database-and-data-management)
6. [Contributors](#contributors)

## VR Development

### VR Software Development

**Game Engines:**
- **Unity3D:** C# scripting

## AI/ML Model Development

### Frameworks and Libraries

**Machine Learning:**
- **TensorFlow:** For developing and training machine learning models
- **Scikit-learn:** For traditional ML algorithms and data processing
- **Keras:** High-level neural networks API, running on top of TensorFlow

### Natural Language Processing (NLP)

- **spaCy or NLTK:** For text processing and understanding

### Computer Vision

- **OpenCV:** For image and video processing
- **Dlib:** For facial recognition and detection

## Backend Development

### Programming Languages

- **Python:** For AI/ML, data analysis, and backend logic
- **C#:** For Unity3D scripting

### Web Frameworks

- **Django:** For smoother integration with ML models and built-in libraries
- **Node.js with Express:** For JavaScript-based backend development

## Database and Data Management

- **PostgreSQL:** For relational database Management

## Frontend Development

- **React:** will be used for non-VR experiences like login functionalities
- **Three.js:** For 3D graphics in the browser (used to add 3D graphics not directly supported by A-Frame)

## Authors

- [Harshita Gupta](https://gitlab.com/harshitagupta0608)
- [Shreshtha Aggarwal](https://gitlab.com/shreshthaaggarwal2708)
- [Vaishnavi Singh](https://gitlab.com/eyevaishnavisingh)

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are greatly appreciated.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement". Don't forget to give the project a star! Thanks again!

1. Clone the Project
2. Create your Feature Branch (git checkout -b feature/NewFeature)
3. Commit your Changes (git commit -m 'Add some NewFeature')
4. Push to the Branch (git push origin feature/NewFeature)
5. Open a Pull Request

