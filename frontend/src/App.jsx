import {Routes, Route} from 'react-router-dom'
import Therapies from './pages/Therapies'
import HomePage from './pages/HomePage'
import Login from './pages/Login'
import Signup from './pages/SignUp'

const App = () => {
  return (
    <Routes>
      <Route path='/' element={<HomePage />} /> 
      <Route path='/therapies' element={<Therapies />} /> 
      <Route path='/login' element={<Login />} />
      <Route path='/signup' element={<Signup />} />

    </Routes>
  )
}

export default App