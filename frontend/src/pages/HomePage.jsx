import { Link } from 'react-router-dom';
import anxiety from '../assets/anxiety.png';
import depression from '../assets/depression.png';
import stress from '../assets/stress.png';
import therapy from '../assets/therapy.jpg';

function home() {
  return (
    <div className="font-sans text-gray-900 bg-gray-50">
      <header className="flex justify-between items-center p-6 bg-gray-200 shadow">
        <div className="text-2xl font-bold">TheraVerse</div>
        <div className="space-x-4">
        <Link to ="/login"><button className="px-4 py-2 bg-yellow-300 rounded hover:bg-yellow-400">Login</button></Link>
        <Link to="/signup"><button className="px-4 py-2 bg-yellow-200 rounded hover:bg-yellow-300">Sign Up</button></Link>
        </div>
      </header>
      <main className="flex-grow p-8">
        <section className="flex flex-col lg:flex-row items-center justify-between mb-12 space-y-8 lg:space-y-0 lg:space-x-8">
          <div className="lg:w-1/2 text-center lg:text-left">
            <h1 className="text-4xl font-bold mb-4">Welcome to TheraVerse</h1>
            <p className="text-lg mb-6">
              Experience the future of therapy with our cutting-edge AI technology.
              Explore various therapy sessions tailored to your needs and learn how our AI can help you achieve mental well-being.
            </p>
            <Link to="/therapies"><button className="px-6 py-3 bg-yellow-300 rounded hover:bg-yellow-400 text-lg">Get Started</button></Link>
          </div>
          <div className="lg:w-1/2">
            <img src={therapy} alt="Therapy VR" className="mx-auto rounded-lg shadow-lg" />
          </div>
        </section>
        <section>
          <h2 className="text-3xl font-bold text-left mb-8">Explore Therapy Sessions</h2>
          <div className="flex flex-wrap justify-center gap-8">
            <div className="bg-gray-200 p-6 rounded-lg shadow-lg w-80">
              <img src={anxiety} alt="Anxiety Relief" className="rounded-lg mb-4" />
              <h3 className="text-xl font-semibold mb-2">Anxiety Relief</h3>
              <p>Experience a calm and serene environment designed to help alleviate anxiety symptoms.</p>
            </div>
            <div className="bg-gray-200 p-6 rounded-lg shadow-lg w-80">
              <img src={depression} alt="Depression Management" className="rounded-lg mb-4" />
              <h3 className="text-xl font-semibold mb-2">Depression Management</h3>
              <p>Engage in activities and environments designed to uplift your mood and provide comfort.</p>
            </div>
            <div className="bg-gray-200 p-6 rounded-lg shadow-lg w-80">
              <img src={stress} alt="Stress Reduction" className="rounded-lg mb-4" />
              <h3 className="text-xl font-semibold mb-2">Stress Reduction</h3>
              <p>Relax in a virtual environment crafted to reduce stress and promote relaxation.</p>
            </div>
          </div>
        </section>
      </main>
      <footer className="bg-gray-200 p-6 text-left shadow mt-12">
        <div className="space-x-6">
          <a href="#" className="hover:underline">About Us</a>
          <a href="#" className="hover:underline">Contact</a>
          <a href="#" className="hover:underline">Privacy</a>
        </div>
      </footer>
    </div>
  );
}

export default home;
