import React from 'react';
import './login.css';

const Signup = () => {
  const publicUrl = import.meta.env.VITE_PUBLIC_URL || '';
  return (
    <div className="login-container">
      <div className="left-panel">
        <div className="logo">
          <img className="icon" src={`${publicUrl}/Icon.png`} alt="Icon" />
          <text className='text'>THERAVERSE</text>
        </div>
        <div className="illustration">
          <img src={`${publicUrl}/Image.png`} alt="Illustration" />
        </div>
      </div>
      <div className="right-panel">
        <h2>Sign Up</h2>
        <br></br>
        <br></br>
        <br></br>
        <form>
          <div className="input-group">
            <label htmlFor="username">Username</label>
            <input type="text" id="username" placeholder="Enter Username" />
          </div>
          <br></br>
          <div className="input-group">
            <label htmlFor="email">Email</label>
            <input type="email" id="email" placeholder="Enter Email" />
          </div>
          <br></br>
          <div className="input-group">
            <label htmlFor="password">Password</label>
            <input type="password" id="password" placeholder="Enter Password" />
          </div>
          <br></br>
          <div className="input-group">
            <label htmlFor="confirm-password">Confirm Password</label>
            <input type="password" id="confirm-password" placeholder="Confirm Password" />
          </div>
          <br></br>
          <br></br>
          <br></br>
          <button type="submit">Sign Up</button>
        </form>
      </div>
    </div>
  );
};

export default Signup;