const Therapies = () => {
    const therapies = [
        {
            title: "Fear Therapy",
            description: "Overcome your fears with guided VR sessions designed to ease your anxiety.",
            buttonLabel: "Start Session",
        },
        {
            title: "Pain Management",
            description: "Utilize VR to manage and reduce chronic pain through immersive experiences.",
            buttonLabel: "Start Session",
        },
        {
            title: "Physical Rehabilitation",
            description: "Engage in physical therapy exercises with VR to aid in rehabilitation.",
            buttonLabel: "Start Session",
        },
    ];

    return (
        <div className="min-h-screen flex flex-col bg-white">
            <header className="bg-yellow-200 py-4">
                <div className="container mx-auto flex justify-between items-center">
                    <h1 className="text-2xl font-bold">TheraVerse</h1>
                    <nav>
                        <a href="/" className="text-black">Home</a>
                    </nav>
                </div>
            </header>
            <main className="container mx-auto py-8 flex-grow">
                <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-8">
                    {therapies.map((therapy, index) => (
                        <div key={index} className="bg-white rounded-lg shadow-md p-6 transform transition-transform duration-300 hover:scale-105">
                            <h2 className="text-xl font-bold mb-4">{therapy.title}</h2>
                            <p className="text-gray-700 mb-4">{therapy.description}</p>
                            <button className="bg-black text-white py-2 px-4 rounded hover:bg-gray-700">
                                {therapy.buttonLabel}
                            </button>
                        </div>
                    ))}
                </div>
            </main>
            <footer className="bg-yellow-200 py-4 mt-auto">
                <div className="container mx-auto flex justify-between items-center">
                    <span className="text-black">Privacy</span>
                    <span className="text-black">Contact</span>
                </div>
            </footer>
        </div>
    );
}

export default Therapies;
